package programa;

import java.util.Scanner;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//creacion de escaner
		Scanner escaner = new Scanner(System.in);
		//creacion de instancia
		clases.GestordeEntrada gestor = new clases.GestordeEntrada();
		//variabe para menu
		int opcion;
		//comienzo do while
		do {
			//menu
		System.out.println("*****************MENU**************");
		System.out.println("1- Diferentes opciones Cliente");
		System.out.println("2- Diferentes opciones Entradas ");
		System.out.println("3- Diferentes opciones Entradas Fisicas ");
		System.out.println("4- Diferentes opciones Entradas Online ");
		System.out.println("5- Extras");
		System.out.println("6- Salir");
		System.out.println("Introduce una opcion:");
		opcion = escaner.nextInt();
		
		
		// el switch  del menu principal
		switch (opcion) {
		
		//caso 1 clientes
		case 1:
			//variable opcion sub menu 1 
			int opcion1;
			//bucle primer sub menu 
			do {
				//sub menu
			System.out.println("********SUBMENU CLIENTE*********");
			System.out.println("1-Dar de alta cliente");
			System.out.println("2-Listar Cliente");
			System.out.println("3-Buscar Cliente");
			System.out.println("4-Eliminar Cliente");
			System.out.println("5-Salir");
			System.out.println("Introduce una opcion:");
			opcion1= escaner.nextInt();
			//switch del primer submenu
			switch (opcion1){
			//	dar de alta cliente
				case 1:
					//pedir los datos por teclado
					System.out.println("Has selecionado la opcon de dar de alta a cliente");
					System.out.println("Introduce el nombre del cliente:");
					escaner.nextLine();
					String nombreCliente = escaner.nextLine();
					System.out.println("Introduce el apellido del cliente:");
					String apellidoCliente = escaner.nextLine();
					System.out.println("Introduce el dni del cliente:");
					String dniCliente = escaner.nextLine();
					System.out.println("Introduce el telefono del cliente:");
					int telefonoCliente = escaner.nextInt();
					//llamar a los metodos de alta y listar cliente
					gestor.altaCliente(nombreCliente, apellidoCliente, dniCliente, telefonoCliente);
					gestor.listarCliente();
				break;
				
				//segundo caso listar cliente
				case 2:
					System.out.println("Has selecionado la opcion de listar clientes");
					//lllamar a metodo  listar cliente
					gestor.listarCliente();
				break;
				
				
				case 3:
					//tercer caso Pedir el cliente que se va a buscar
					System.out.println("Has selecionado la opcion de buscar cliente");
					System.out.println("Introduce el dni del cliente");
					escaner.nextLine();
					String dniBusqueda = escaner.nextLine();
					//llamar al metodo
					gestor.buscarCliente(dniBusqueda);
				break;
				
				case 4:
					//cuarto caso eliminar cliente 
					//introducir cliente por teclado
					System.out.println("Has selecionado la opcion de eliminar cliente");
					System.out.println("Introduce el dni del cliente que quieres eliminar");
					escaner.nextLine();
					String dniEliminado = escaner.nextLine();
					//llamar al metodo eliminar cliente
					gestor.eliminarCliente(dniEliminado);
					gestor.listarCliente();
					
				break;
				
				// caso 5 salir
				case 5:
						System.out.println("Has selecionado la opcion de salir");
					
					break;
			
			}
			//terminar bucle submenu
			}while(opcion1!=5);
				
			
		break;
			
			
		//sub menu 2
		case 2: 
			//variable sub menu 2
			int opcion2;
			do {
				//submenu 2
			System.out.println("********SUBMENU ENTRADAS*********");
			System.out.println("1-Dar de alta entrada");
			System.out.println("2-Listar entrada");
			System.out.println("3-Buscar entrada");
			System.out.println("4-Eliminar entrada");
			System.out.println("5-Salir");
			System.out.println("Introduce una opcion:");
			opcion2=escaner.nextInt();
			//switch sub menu 2
			switch(opcion2) {
			
			//caso 1dar de alta entrada
				case 1:
					//pedir datos por tecado
					System.out.println("Has selecionado la opcion de dar de alta una entrada");
					System.out.println("Introduce el codigo de la entrada");
					escaner.nextLine();
					String codEntrada = escaner.nextLine();
					System.out.println("Introduce la fecha de compra de la entrada (2020-11-20 ejemplo)");
					String fechaCompra = escaner.nextLine();
					System.out.println("Introduce el precio de la entrada");
					double precio = escaner.nextDouble();
					escaner.nextLine();
					//llamar a metodos alta entrada y listar 
					gestor.altaEntrada(codEntrada, fechaCompra, precio);
					gestor.listarEntradas();
					
				break;
			
				//caso 2 listar entradas
				case 2:
					System.out.println("Has selecionado la opcion de listar entradas");
					gestor.listarEntradas();
				break;
					
					//caso 3 buscar entrada y pedir datos por teclado de la entrada
				case 3:
					System.out.println("Has selecionado la opcion de buscar entrada");
					System.out.println("Introduce el codigo de la entrada que quieras buscar");
					escaner.nextLine();
					String buscarcodEntrada = escaner.nextLine();
					//llamar a metodo
					gestor.buscarEntrada(buscarcodEntrada);
				break;
				case 4:
					//caso 4 eliminar entrada y pedir enrada por teclado 
					System.out.println("Has selecionado la opcion de eliminar entrada");
					System.out.println("Introduce el codigo de la entrada que quieras eliminar");
					escaner.nextLine();
					String eliminarEntrada = escaner.nextLine();
					//llamar a metodos eliminar y listar
					gestor.eliminarEntrada(eliminarEntrada);
					gestor.listarEntradas();
				break;
				//caso 5 opcion salir
				case 5:
					System.out.println("Has selecionado la opcion de salir");
					
				break;
		
			}
			//fin bucle sub menu 2
			
			}while(opcion2!=5);
		break;
			
			
			
		case 3:
			//submenu 3 y variable de el submenu 3
			int opcion3;
			do {
				//submenu entradas fisicas
			System.out.println("********SUBMENU ENTRADA FISICA*********");
			System.out.println("1-Dar de alta Entrada Fisica");
			System.out.println("2-Listar Entradas Fisicas");
			System.out.println("3-Buscar Entrada Fisica");
			System.out.println("4-Eliminar Entrada Fisica");
			System.out.println("5-Salir");
			System.out.println("Introduce una opcion:");
			opcion3 = escaner.nextInt();
			//switch sub menu 3
			switch (opcion3) {
			//caso 1 dar de alta y pedir los datos por teclado de una entrada fisica
			case 1:
				System.out.println("Has selecionado la opcion de dar de alta una entrada Fisica");
				System.out.println("Introduce el codigo de la entrada");
				escaner.nextLine();
				String codEntrada = escaner.nextLine();
				System.out.println("Introduce la fecha de compra de la entrada (2020-11-20 ejemplo)");
				String fechaCompra = escaner.nextLine();
				System.out.println("Introduce el precio de la entrada");
				double precio = escaner.nextDouble();
				escaner.nextLine();
				System.out.println("Introduce el codigo postal:");
				int cp = escaner.nextInt();
				System.out.println("Introduce el numero de socio:");
				int socio = escaner.nextInt();
				System.out.println("Introduce el tipo de pago (Efectivo o tarjeta)");
				escaner.nextLine();
				String pago = escaner.nextLine();
				
				//llamar a los metodos dar de alta entrada fisica y listar
				gestor.altaEntradaFisica(codEntrada, fechaCompra, precio, cp, socio, pago);
				gestor.listarEntradaFisica();
				break;
			
			case 2:
				//caso 2 listar entradas fisicar y llamar a metodo listar
				System.out.println("Has selecionado la opcion de listar entradas fisicas");
				gestor.listarEntradaFisica();
				
			break;
			
			
			
			case 3:
				//caso 3 buscar entrada fisica e introducir datos por teclado
				System.out.println("Has selecionado la opcion de buscar entrada fisica");
				System.out.println("Introduce el codigo de la entrada que quieres buscar:");
				escaner.nextLine();
				String codEntradaaFisica = escaner.nextLine();
				//llamar a metodo buscar
				gestor.buscarEntradaFisicas(codEntradaaFisica);
				
			break;
			
			
			case 4:
				//caso 4 elimiar entrada fisica y pedir los datos de la entrada fisica por teclado 
				System.out.println("Has selecionado la opcion de eliminar entrada fisica");
				System.out.println("Introduce el codigo de la entrada fisica que quieres eliminar");
				escaner.nextLine();
				String codEntradaFisica = escaner.nextLine();
				//llamar a metodos eliminar y listar
				gestor.eliminarEntradaFisicas(codEntradaFisica);
				gestor.listarEntradaFisica();
				
				
			break;
			
			
			case 5:
				//caso 5 salir
				System.out.println("Has selecionado la opcion de salir");
		
			break;
			
			}
			
			//fin bucle sub menu 3
			}while(opcion3!=3);
			
			
			
		break;
			
			
			
			
		case 4:
			//submenu 4 y variable para el submenu
			int opcion4;
			//bucle
			do{
				//menu
			System.out.println("********SUBMENU ENTRADA ONLINE*********");
			System.out.println("1-Dar de alta Entrada Online");
			System.out.println("2-Listar Entradas Online");
			System.out.println("3-Buscar Entradas Online");
			System.out.println("4-Eliminar Entradas Online");
			System.out.println("5-Salir");
			System.out.println("Introduce una opcion:");
			opcion4 = escaner.nextInt();
			
			//switch submenu
			switch (opcion4) {
			
				case 1:
					//caso 1 dar de alta entrada fisica y pedir los datos por teclado
					System.out.println("Has selecionado la opcion de dar de alta una entrada Fisica");
					System.out.println("Introduce el codigo de la entrada");
					escaner.nextLine();
					String codEntrada = escaner.nextLine();
					System.out.println("Introduce la fecha de compra de la entrada (2020-11-20 ejemplo)");
					String fechaCompra = escaner.nextLine();
					System.out.println("Introduce el precio de la entrada");
					double precio = escaner.nextDouble();
					escaner.nextLine();
					System.out.println("Introduce el email:");
					String email = escaner.nextLine();
					System.out.println("Introduce la direccion:");
					String direccion = escaner.nextLine();
					System.out.println("Introduce el descuento");
					String descuento = escaner.nextLine();
					System.out.println("Introduce el numero de cuenta");
					String numeroCuenta = escaner.nextLine();
				
					//llamar a los metodos
					gestor.altaEntradaOnline(codEntrada, fechaCompra, precio, email, direccion, descuento, numeroCuenta);
					gestor.listarEntradaOnline();
					
				break;
					
					//listar las entradas y llamar al metodo
				case 2:
					System.out.println("Has seleccionado la opcion de listar entradas online");
					gestor.listarEntradaOnline();
					
				break;
				
				case 3:
					// caso 3 buscar entradas online  y pedir por teclado el cod entrada que se va a buscar
					System.out.println("Has selecionado la opcion de buscar entrada online");
					System.out.println("Introduce el codigo de la entrada que quieres buscar:");
					escaner.nextLine();
					String codEntradaOnline = escaner.nextLine();
					//llamar al metodo
					gestor.buscarEntradaOnline(codEntradaOnline);
				break;
				
				case 4:
					//caso 4 eliminar entrada online y pedir por teclado el cod de la entrada eliminada
					System.out.println("Has selecionado la entrada de eliminar entrada");
					System.out.println("Introduce el codigo de la entrada que quieres eliminar:");
					escaner.nextLine();
					String codEntradaOnlineEliminar = escaner.nextLine();
					
					//llamar a metodos eliminar y listar 
					gestor.eliminarEntradaOnline(codEntradaOnlineEliminar);
					gestor.listarEntradaOnline();
				break;
				
				
				
				case 5:
					//caso 5 opcion de salir 
					System.out.println("Has selecionado la opcion de salir");
					
				break;
			
			}
			//fin de bucle submenu4
			}while(opcion4!=5);
			
		break;
			
			
			//sub menu extras
		case 5:
			//variable para submenu
			int opcion5;
			do {
				//submenu
			 System.out.println("**********SUBMENU EXTRAS*************");
			 System.out.println("1-Entrada mas cara");
			 System.out.println("2- Media de precio entradas");
			 System.out.println("3- Salir");
			 System.out.println("Introduce una opcion:");
			 opcion5 = escaner.nextInt();
			 //switch
			 switch(opcion5){
			
			case 1:
				//caso 1 mostrar el precio de la entrada mas cara
				System.out.println("Has selecionado la opcion de la entrada mas cara");
				 gestor.maxEntradaPrecio();
				 
			break;
			 
			case 2:
				//caso 2 mostrar la media de las entradas
				 System.out.println("Has selecionado la opcion de media de precio de enradas");
				 gestor.mediaEntrada();
			break;
			
			
			//salir
			case 3:
				 System.out.println("Has selecionado la opcion de salir ");
				 
			break;
		
					 
			 
			 
			 }
			 //fin de bucle de submenu
			}while(opcion5!=3);
		break;
		
		case 6:
			//caso 6 opcion de salir del menu principal
			System.out.println("Has selecionado la opcion de salir");
			System.exit(0);
			
			
			
		break; 
			
		
		}
		//fin de bucle del menu principal
		
		}while(opcion!=6);
		
		//cerrar escaner
		
		escaner.close();
		
		
	}

}
