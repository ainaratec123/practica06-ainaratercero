package clases;


//sub clase de entrada
public class EntradaOnline extends Entrada {

	//atributos
	private String email;
	private String direccion;
	private String descuento;
	private String numeroCuenta;
	 
	//constructor
	public EntradaOnline(String codEntrada) {
		
		super(codEntrada);	
		
	}

	
	//getter y setter

	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getDescuento() {
		return descuento;
	}

	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}


	public String getNumeroCuenta() {
		return numeroCuenta;
	}


	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}


//to String
	@Override
	public String toString() {
		return super.toString() +"EntradaOnline [email=" + email + ", direccion=" + direccion + ", descuento=" + descuento
				+ ", numeroCuenta=" + numeroCuenta + "]";
	}

	//metodo 

	public double seguro(double suplemento) {
		precio+=1.30;
		return precio;
	}
	
	
	
	


	

	
	
	
	
	
	
	
	
	
}
