package clases;
//subclase
public class EntradaFisica extends Entrada{

//atributos	 
	  private int  codigoPostal;
	  private int numeroSocio;
	  private String tipoPago;
	
	  //constructor
	  public EntradaFisica(String codEntrada) {
			
			super(codEntrada);	
			
		}
	
	//getter y setter
	public int getCodigoPostal() {
		return codigoPostal;
	}


	public void setCodigoPostal(int codigoPostal) {
		this.codigoPostal = codigoPostal;
	}


	public int getNumeroSocio() {
		return numeroSocio;
	}


	public void setNumeroSocio(int numeroSocio) {
		this.numeroSocio = numeroSocio;
	}


	public String getTipoPago() {
		return tipoPago;
	}


	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}


	//to String
	@Override
	public String toString() {
		return super.toString() + "EntradaFisica [codigoPostal=" + codigoPostal + ", numeroSocio=" + numeroSocio + ", tipoPago=" + tipoPago
				+ "]";
	}

//metodo
	public double seguro(double suplemento) {
		precio+=3.25;
		return precio;
	}


	
	
	
}
