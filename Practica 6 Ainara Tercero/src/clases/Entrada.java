package clases;

import java.time.LocalDate;

public class Entrada {
	
	//atributos
	String codEntrada;
	LocalDate fechaConcierto;
	LocalDate fechaCompra;
	double precio;
	
	//constructor
	public Entrada (String codEntrada) {
		
		this.codEntrada= codEntrada;
		
	}
	
	//gettter y setter
	
	public String getCodEntrada() {
		return codEntrada;
	}
	public void setCodEntrada(String codEntrada) {
		this.codEntrada = codEntrada;
	}
	public LocalDate getFechaConcierto() {
		return fechaConcierto;
	}
	public void setFechaConcierto(LocalDate fechaConcierto) {
		this.fechaConcierto = fechaConcierto;
	}
	public LocalDate getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(LocalDate fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	
	//toString
	@Override
	public String toString() {
		return "Entrada [codEntrada=" + codEntrada + ", fechaConcierto=" + fechaConcierto + ", fechaCompra="
				+ fechaCompra + ", precio=" + precio + "]";
	}
	
	
	
	//metodo
	
	public double seguro(double suplemento) {
		precio+=suplemento;
		return precio;
	}
	
	
	
	
	
	

}
