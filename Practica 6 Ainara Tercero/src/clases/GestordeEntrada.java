package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestordeEntrada {

	//creacion de arrys
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Entrada> listaEntradas;
	private ArrayList<EntradaFisica> listaEntradasFisicas;
	private ArrayList<EntradaOnline> listaEntradasOnline;
	
	//constructor
	
	public GestordeEntrada() {
		
		listaClientes = new ArrayList<Cliente>();
		listaEntradas = new ArrayList<Entrada>();
		listaEntradasFisicas = new ArrayList<EntradaFisica>();
		listaEntradasOnline = new ArrayList<EntradaOnline>();
	
	}
	
	
	//alta cliente
	public void altaCliente(String nombre, String apellido, String dni, int telefono) {
		if (!existeCliente(dni)) {
			Cliente nuevoCliente = new Cliente (nombre);
			nuevoCliente.setApellido(apellido);
			nuevoCliente.setDni(dni);
			nuevoCliente.setTelefono(telefono);
			listaClientes.add(nuevoCliente);
		}
		
		else {
			System.out.println("El cliente ya esta registrado");
		}
		
	}
	
	//Comprobar si existe cliente
	 public boolean existeCliente(String dni) {
		 for (Cliente cliente : listaClientes) {
			 if (cliente != null & cliente.getDni().equals(dni)) {
				 return true;
			 }
		 }
		 
		 return false;
	 }
	 
	
	
	 //metodo listar clientes
	 public void listarCliente() {
		 
		 
		 for (int i =0; i<listaClientes.size(); i++) {
			 Cliente cliente = listaClientes.get(i);
			 if (cliente != null) {
				 System.out.println(cliente);
			 }
		 } 
	 }
	 
	
	 //metodo buscar cliente
	 public Cliente buscarCliente(String dni) {
			for (Cliente cliente : listaClientes) {
				if (cliente != null && cliente.getDni().equals(dni)) {
					System.out.println(cliente);
					return cliente;
				}
			}
			System.out.println("No esta registrado ningun cliente con ese dni");
			return null;
		}
	 
	//metodo eliminar cliente
	public void eliminarCliente(String dni) {
		Iterator<Cliente> iteratorCliente = listaClientes.listIterator();
	
		
		while(iteratorCliente.hasNext()) {
			Cliente cliente = iteratorCliente.next();
			if (cliente.getDni().equals(dni)) {
				iteratorCliente.remove();
			}
		}
		
	}
	
	
	
	//alta entrada
	
	
	public void altaEntrada(String codEntrada,String fechaCompra,  double precio) {
		
		if (!existeEntrada(codEntrada)) {
			Entrada nuevaEntrada = new Entrada(codEntrada);
			nuevaEntrada.setFechaConcierto(LocalDate.now());
			nuevaEntrada.setFechaCompra(LocalDate.parse(fechaCompra));
			nuevaEntrada.setPrecio(precio);
			listaEntradas.add(nuevaEntrada);
		}
		else {
		System.out.println("La entrada ya esta vendida");
		}
	}
	
	//metodo si existe entrada
	public boolean existeEntrada(String codEtrada) {
		for (Entrada entrada :listaEntradas) {
			if (entrada != null && entrada.getCodEntrada().equals(codEtrada)) {
				return true;
			
				}
			}
		return false;
	}
	
	
	//metodo listar entradas
		public void listarEntradas() {
			
			for (int i =0; i<listaEntradas.size(); i++) {
				 Entrada entrada = listaEntradas.get(i);
				 if (entrada != null) {
					 System.out.println(entrada);
				 }
			 } 
			
		}
		
		//metodo buscar entrada
		public Entrada buscarEntrada(String codEntrada) {
			
			for (int i =0; i<listaEntradas.size(); i++) {
				Entrada entrada= listaEntradas.get(i);
				if ( entrada != null && entrada.getCodEntrada().equals(codEntrada)) {
					System.out.println(entrada);
					return entrada;
				}
			}
			System.out.println("La entrada buscada no existe");
			return null;
		}
		
		//metodo eliminar entradas
		public void eliminarEntrada(String codEntrada) {
			
			Iterator<Entrada> iteratorEntrada = listaEntradas.listIterator();
		
		while (iteratorEntrada.hasNext()) {
			Entrada entrada = iteratorEntrada.next();
				if (entrada.getCodEntrada().equals(codEntrada)) {
					iteratorEntrada.remove();
				}		
			}	
		}
		
		//metodo dar de alta entradas fisicas
		public void altaEntradaFisica(String codEntrada,String fechaCompra,  double precio,
				int codigoPostal, int numeroSocio, String tipoPago ) {
			
			if (!existeEntradaFisica(codEntrada)) {
				EntradaFisica nuevaEntradaFisica = new EntradaFisica(codEntrada);
				nuevaEntradaFisica.setFechaConcierto(LocalDate.now());
				nuevaEntradaFisica.setFechaCompra(LocalDate.parse(fechaCompra));
				nuevaEntradaFisica.setPrecio(precio);
				nuevaEntradaFisica.setCodigoPostal(codigoPostal);
				nuevaEntradaFisica.setNumeroSocio(numeroSocio);;
				nuevaEntradaFisica.setTipoPago(tipoPago);
				listaEntradasFisicas.add(nuevaEntradaFisica);
			}
			else {
		
			System.out.println("La entrada ya esta vendida");
			}
	
		}
		//metodo si existe entrada fisica
		public boolean existeEntradaFisica(String codEtrada) {
			for (EntradaFisica fisica :listaEntradasFisicas) {
				if (fisica != null && fisica.getCodEntrada().equals(codEtrada)) {
					return true;
				
					}
				}
			return false;
		}
		
		
	
		//metodo listar enradas fisicas
		
		 public void listarEntradaFisica() {
			 
			 
			 for (int i =0; i<listaEntradasFisicas.size(); i++) {
				 EntradaFisica fisica = listaEntradasFisicas.get(i);
				 if (fisica!= null) {
					 System.out.println(fisica);
				 }
			 } 
		 }
		 
		//metodo buscar entradas fisicas 
		 public Entrada buscarEntradaFisicas(String codEntrada) {
				
				for (int i =0; i<listaEntradasFisicas.size(); i++) {
					EntradaFisica fisica= listaEntradasFisicas.get(i);
					if ( fisica != null && fisica.getCodEntrada().equals(codEntrada)) {
						System.out.println(fisica);
						return fisica;
					}
				}
				System.out.println("La entrada buscada no existe");
				return null;
			}
	
		 //metodo eliminar entrada fisica
		 public void eliminarEntradaFisicas(String codEntrada) {
				
				Iterator<EntradaFisica> iteratorEntrada = listaEntradasFisicas.listIterator();
			
			while (iteratorEntrada.hasNext()) {
				EntradaFisica fisica = iteratorEntrada.next();
					if (fisica.getCodEntrada().equals(codEntrada)) {
						iteratorEntrada.remove();
					}		
				}	
			}
			
		 
		 //metodo dar de alta entradas online
		 
		 public void altaEntradaOnline(String codEntrada,String fechaCompra,  double precio,
					String email, String direccion, String descuento, String numeroCuenta ) {
				
				if (!existeEntradaOnline(codEntrada)) {
					EntradaOnline nuevaEntradaOnline = new EntradaOnline(codEntrada);
					nuevaEntradaOnline.setFechaConcierto(LocalDate.now());
					nuevaEntradaOnline.setFechaCompra(LocalDate.parse(fechaCompra));
					nuevaEntradaOnline.setPrecio(precio);
					nuevaEntradaOnline.setEmail(email);
					nuevaEntradaOnline.setDireccion(direccion);
					nuevaEntradaOnline.setDescuento(descuento);
					nuevaEntradaOnline.setNumeroCuenta(numeroCuenta);
					listaEntradasOnline.add(nuevaEntradaOnline);
				}
				else {
			
				System.out.println("La entrada ya esta vendida");
				}
		
			}
			//metodo si existe entrada online
			public boolean existeEntradaOnline(String codEtrada) {
				for (EntradaOnline online :listaEntradasOnline) {
					if (online != null && online.getCodEntrada().equals(codEtrada)) {
						return true;
					
						}
					}
				return false;
			}
			
			
		
			//metodo listar entradas online
			 public void listarEntradaOnline() {
				 
				 
				 for (int i =0; i<listaEntradasOnline.size(); i++) {
					 EntradaOnline online = listaEntradasOnline.get(i);
					 if (online!= null) {
						 System.out.println(online);
					 }
				 } 
			 }
			 
			 //metodo buscar entrada online
			 public Entrada buscarEntradaOnline(String codEntrada) {
					
					for (int i =0; i<listaEntradasOnline.size(); i++) {
						EntradaOnline online= listaEntradasOnline.get(i);
						if ( online != null && online.getCodEntrada().equals(codEntrada)) {
							System.out.println(online);
							return online;
						}
					}
					System.out.println("La entrada buscada no existe");
					return null;
				}
		
			 
			 //metodo eliminar entrada online
			 
			 
			 public void eliminarEntradaOnline(String codEntrada) {
					
					Iterator<EntradaOnline> iteratorEntrada = listaEntradasOnline.listIterator();
				
				while (iteratorEntrada.hasNext()) {
					EntradaOnline online = iteratorEntrada.next();
						if (online.getCodEntrada().equals(codEntrada)) {
							iteratorEntrada.remove();
						}		
					}	
				}
			 
			
		//metodo precio maximo de entradas	 
			 
			 public void maxEntradaPrecio() {
					double max =0;
					for (int i =0; i<listaEntradas.size(); i++) {
						Entrada entrada = listaEntradas.get(i);
						if ( entrada.getPrecio()>max ) {
							max = entrada.getPrecio();
							
						}
					}
					System.out.println("El precio maximo es " + max);
				}
			 
			 
			//metodo metia precio entradas 
			 
			 public void mediaEntrada() {
					double total =0 ;
					for (Entrada entrada : listaEntradas) {
						if (entrada != null) {
							total = total + entrada.getPrecio();
						}
					}
					
					System.out.println(total/listaEntradas.size());
				}
			 
			 
			
			 
			 
		 
		 
		 
		 
		 
}
