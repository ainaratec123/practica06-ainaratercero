package clases;

public class Cliente {
	
	//atributos
	private String nombre;
	private String apellido;
	private String dni;
	private int telefono;
	//constructor
	public Cliente (String nombre) {
		
		this.nombre= nombre;
		
	}
	
	//getter y setter

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}


//to String
	@Override
	public String toString() {
		return "Cliente [nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni + ", telefono=" + telefono + "]";
	}
	
	
	
	
	
	
	
	
	
	
	

}
